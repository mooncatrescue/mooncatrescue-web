(function(){
  var content =
      '<div id="deprecation-warning" class="detail-overlay modal-dismiss" style="display:none">\
	  <div id="deprecation-body" class="detail-modal center">\
	    <div class="close modal-dismiss">\
	      <button class="cancel">x<span class="deprecation-timer"></span></button>\
	    </div>\
	    <div class="info-group">\
	      <div class="detail-info center">History Ahead!</div>\
	    </div>\
	    <div class="info-group">\
	      <p>\
		This is the <em>original website</em> for the <em>MoonCatRescue</em> Project from <em>2017</em>.\
	      </p>\
	      <p>\
		As one of the <em>earliest NFTs</em>, we feel it is important to keep the origins intact. So, we are leaving this website as is, but consider it a <em>historical artifact</em>\
	      </p>\
	      <p>\
		<em>Adopt</em> a MoonCat on <a href="https://opensea.io/collection/acclimatedmooncats">OpenSea</a>\
	      </p>\
	      <p>\
		Follow <em>new developments</em> on the Official <a href="https://mooncat.community">MoonCatCommunity</a> site\
	      </p>\
	    </div>\
	    <div class="info-group">\
	      <button class="select modal-dismiss">Got it<span class="deprecation-timer"></span></button>\
	    </div>\
	  </div>\
	</div>'
  var $container = document.createElement("div")
  $container.innerHTML=content;
  document.body.append($container);

  var interval = 1000 * 60 * 60 * 24 * 5; // don't nag for 5 days
  var $modal = $('#deprecation-warning');
  var $timers = $('.deprecation-timer');
  $('#deprecation-body').on('click', function(e){
    e.stopPropagation();
  })
  var countdownSeconds = 3;
  function countdown(){
    if(countdownSeconds == 0){
      $('.modal-dismiss').each(function(n, el){
	el.addEventListener('click', function(){
	  localStorage.setItem('deprecation-banner', Date.now());
	  $modal.attr("style", "display:none;");
	})
      })
      $timers.each(function(n, el){$(el).parent().attr("style", "");el.innerText="";})
    }else{
      $timers.each(function(n, el){$(el).parent().attr("style", "background-color:grey"); el.innerText=" ("+countdownSeconds+")";})
      countdownSeconds--;
      setTimeout(countdown, 1000)
    }
  }
  var ts = parseInt(localStorage.getItem("deprecation-banner") || "0", 10);
  if(Date.now() > (ts + interval)){
    countdown();
    $modal.attr("style", "display:block;");
  }
  window.nag = function(){
    localStorage.removeItem("deprecation-banner");
    window.location.reload();
  }

})()
