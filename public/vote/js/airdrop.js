function determineAirdropTargets (numberOfTargets, eligibleCandidatesArray, blockHashesArray){
    // This function takes a number of airdrop targets, an array of eligible candidates,
    // and an array of block hashes. It returns an array of target addresses selected from the
    // eligibleCandidatesArray which contains a maximum of numberOfTargets elements. It may contain
    // fewer if too few blockHashes are provided in the blockHashesArray, in which case it should
    // be run again with additional block hashes.

    // Each block hash is used to generate a number which represents an index into the eligibleCandidatesArray
    // if a specific block hash computes to an index that is out of range, or an index that has already
    // appeared, it is discarded.

    // The indexes are computed by splitting the block hash into equal-length segments of bits which
    // are then XORed together. The minimum bit segment length is used, and any leftover bits are discarded.
    // Because the number of targets is unlikely to be a power of two, some generated indexes will be out
    // of range and must be discarded.



    // First we need to define some helper functions:

    // This function determines the minimum number of bits required to represent all indexes
    // in the eligibleCandidatesArray.
    function determineMinBitSegmentSize(numberOfCandidates){
	var bitSegmentSize = 1; // start with a size of zero
	while (Math.pow(2, bitSegmentSize) < numberOfCandidates){
	    // we increase the size by one until we have enough bits
            // to represent the number of candidates
	    bitSegmentSize++;
	}
	return bitSegmentSize; // return the final bit segment size
    }

    // This is a lookup table which is used to turn hexidecimal characters into bit strings.
    var HexCharToBitString = {"0": "0000", "1": "0001", "2": "0010", "3": "0011",
			      "4": "0100", "5": "0101", "6": "0110", "7": "0111",
			      "8": "1000", "9": "1001", "a": "1010", "b": "1011",
			      "c": "1100", "d": "1101", "e": "1110", "f": "1111"};

    // This function simply converts a hexidecimal string to a string of bits.
    function convertHexStringToBitString(hexString){

	if(hexString.slice(0,2) == "0x"){   // if the hexString is prefixed with '0x'
	    hexString = hexString.slice(2); // remove it
	}

	var fullBitString = ""; // starts empty

	for(var i = 0; i < hexString.length; i++){       // go through each character of the hex string
	    var hexChar = hexString[i];                  // identify the hex character
	    var bitString = HexCharToBitString[hexChar]; // fetch the hex character's bit string representation
	    fullBitString = fullBitString + bitString;   // append the bit string to the full bit string
	}

	return fullBitString; // return the full resulting bit string
    }

    // This function turns a block hash into a random number by splitting it into equal-length segments
    // of a certain number of bits and XORing those segments together. Leftover bits are discarded
    function getDerivedRandomNumber(bitSegmentSize, blockHashHexString){
	var blockHashBitString = convertHexStringToBitString(blockHashHexString);

	// we will cut off any bits that do not fill a full segment
	var maxBitStringIndex = blockHashBitString.length - (blockHashBitString.length % bitSegmentSize);

	var result = 0; // start with a result of 0

	for (var i = 0; i < maxBitStringIndex; i += bitSegmentSize){ // go through the bitstring in segments of bitSegmentSize
	    var bitSegment = blockHashBitString.slice(i, i + bitSegmentSize); // get the next segment
	    var intSegment = parseInt(bitSegment, 2); // parse the segment's bit string into an integer
	    result = result ^ intSegment; // take the current result and XOR it with the segment's integer value
	}

	return result; // return the result
    }

    // This function collects the random numbers associated with each blockhash, discarding
    // the ones which are out of range or are duplicates and returns an array of indexes.
    function deriveUniqueRandomIndexesFromBlockHashes (numberOfCandidates, blockHashesArray){
	var bitSegmentSize = determineMinBitSegmentSize(numberOfCandidates);

	var seenIndexes = {};
	var randomIndexes = []; // collection of

	for(var i = 0; i < blockHashesArray.length; i++){ // go through each blockHash in the blockHashesArray
	    var blockHashHexString = blockHashesArray[i]; // get the block hash at the index
	    var randomIndex = getDerivedRandomNumber(bitSegmentSize, blockHashHexString); // derive the random Index
	    if(randomIndex < numberOfCandidates && !seenIndexes[randomIndex]){
		// above, we ensure the randomIndex represents a candidate
		// by checking that it is less than the total number of candidates
		// and that it is not a duplicate
		seenIndexes[randomIndex] = true   // then we put it into seenIndexes
		randomIndexes.push(randomIndex);  // and add the random index to result array
	    } // otherwise it is not included in the result
	}
	return randomIndexes;
    }

    // Finally we pull it all together:

    // We start by sorting the candidate address list lexicographically
    eligibleCandidatesArray.sort();
    // We turn the blockhashes into indexes that will reference the array of eligible candidate addresses.
    var randomIndexArray = deriveUniqueRandomIndexesFromBlockHashes(eligibleCandidatesArray.length, blockHashesArray);
    // We then get our target indexes by taking only as many indexes as there are targets.
    var targetIndexArray = randomIndexArray.slice(0, numberOfTargets);
    // Finally we fetch the candidate address for each target index
    var targetArray = targetIndexArray.map(function(index){ return eligibleCandidatesArray[index]; });
    // and return the result.
    return targetArray;
}
